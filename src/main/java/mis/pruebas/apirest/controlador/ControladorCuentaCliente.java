package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentaCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCuenta servicioCuenta;

    public static class DatosEntradaCuenta {
        public  String codigoCuenta;
    }

    // POST http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody DatosEntradaCuenta datosEntradaCuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, datosEntradaCuenta.codigoCuenta);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    // GET http://localhost:9000/api/v1/clientes/12345678/cuentas
    @GetMapping
    public ResponseEntity<List<String>> obtenerCuentasCliente(@PathVariable String documento) {

        try{
            final var cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch (ObjetoNoEncontrado x) {
            return ResponseEntity.notFound().build();
        }

    }

    // CONSULTA http://localhost:9000/api/v1/clientes/12345678/cuentas/123456***
    @GetMapping("/{numeroCuenta}")
    public ResponseEntity <Cuenta> obtenerCuentaCliente(@PathVariable String documento,
                                                  @PathVariable String numeroCuenta) {
        try{
            final var cuenta = this.servicioCuenta.obtenerCuentaCliente(documento, numeroCuenta);
            return ResponseEntity.ok(cuenta);
        } catch (ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
    }

    // PUT http://localhost:9000/api/v1/clientes/12345678/cuentas + DATOS
    @PutMapping
    public ResponseEntity modificarCuentaCliente(@PathVariable String documento,
                                                 @RequestBody Cuenta nroCuenta) {
        try {

            this.servicioCuenta.modificarCuentaCliente(documento, nroCuenta);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @PatchMapping
    public ResponseEntity emparcharCuentaCliente(@PathVariable String documento,
                                                 @RequestBody Cuenta nroParche) {
        try {
            this.servicioCuenta.emparcharCuentaCliente(documento, nroParche);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.ok().build();
    }


    // DELETE http://localhost:9000/api/v1/clientes/12345678/cuentas/123456***
    @DeleteMapping("/{numeroCuenta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity eliminarCuentasCliente(@PathVariable String documento,
                                               @PathVariable String numeroCuenta){

        try {
            this.servicioCuenta.eliminarCuentaCliente(documento, numeroCuenta);

        } catch(ObjetoNoEncontrado x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, x.getMessage());
        }
        return ResponseEntity.noContent().build();

    }


}
