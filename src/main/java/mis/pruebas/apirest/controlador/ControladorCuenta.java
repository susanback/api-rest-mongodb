package mis.pruebas.apirest.controlador;


import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping(Rutas.CUENTAS)
@CrossOrigin

public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // http://localhost:9000/api/v1/cuentas
    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}

    // http://localhost:9000/api/v1/cuentas + DATOS
    @PostMapping
    public ResponseEntity agregarCuenta(@RequestBody Cuenta cuenta){
        this.servicioCuenta.insertarCuentaNueva(cuenta);

        final var location =linkTo(
                methodOn(ControladorCuenta.class).obtenerunaCuenta(cuenta.numero)
                 ).toUri();

        return ResponseEntity
                .ok() //200 ok
                .location(location) // Location: http://localhost:9000/api/v1/clientes/109343498
                .build(); // Mandar la resupesta.
    }

    // http://localhost:9000/api/v1/cuentas/{documento}
    // http://localhost:9000/api/v1/cuentas/12345678
    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        return this.servicioCuenta.obtenerCuenta(numero);
    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        cuenta.numero = ctanumero;
        this.servicioCuenta.guardarCuenta(cuenta);

    }

    // http://localhost:9000/api/v1/cuentas/{documento} + DATOS
    // http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numero = ctanumero;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    // DELETE http://localhost:9000/api/v1/cuentas/{numero}
    // DELETE http://localhost:9000/api/v1/cuentas/12345678 + DATOS
    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (ObjetoNoEncontrado x) {
        }
    }

}
