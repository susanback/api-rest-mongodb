package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@RequestMapping(Rutas.CLIENTES)

public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    PagedResourcesAssembler pagedResourcesAssembler;

    // GET http://localhost:9000/api/v1/clientes -> List<Cliente> obtenerClientes()
    @GetMapping
    public PagedModel<EntityModel<Cliente>> obtenerClientes(@RequestParam int pagina, @RequestParam int cantidad) {
        try{
            final var pageSinMetadatos = this.servicioCliente.obtenerClientes(pagina, cantidad);
            final var page =pageSinMetadatos.map(
                    x ->EntityModel.of(x).add(
                                    linkTo(methodOn(this.getClass()).obtenerUnCliente(x.documento))
                                            .withSelfRel().withTitle("Este Cliente"),
                                    linkTo(methodOn(ControladorCuentaCliente.class).obtenerCuentasCliente(x.documento))
                                            .withRel("cuentas").withTitle("Cuentas del cliente")
                    ));
            return this.pagedResourcesAssembler.toModel(page);

        } catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // POST http://localhost:9000/api/v1/clientes + DATOS -> agregarCliente(Cliente)
    @PostMapping
    public ResponseEntity agregarCliente(@RequestBody Cliente cliente) {
        this.servicioCliente.insertarClienteNuevo(cliente);

        final var location =linkTo(
                methodOn(ControladorCliente.class).obtenerUnCliente(cliente.documento)
                ).toUri();

        return ResponseEntity
                .ok() //200 ok
                .location(location) // Location: http://localhost:9000/api/v1/clientes/109343498
                .build(); // Mandar la resupesta.

    }

    // GET http://localhost:9000/api/v1/clientes/{documento} -> obtenerUnCliente(documento)
    // GET http://localhost:9000/api/v1/clientes/12345678 -> obtenerUnCliente("12345678")
    @GetMapping("/{documento}")
    public EntityModel<Cliente> obtenerUnCliente(@PathVariable String documento) {
        try {
            final Link self = linkTo(methodOn(this.getClass()).obtenerUnCliente(documento)).withSelfRel();
            final Link cuentas=linkTo(methodOn(ControladorCuentaCliente.class).obtenerCuentasCliente(documento)).withRel("cuentas");
            final Link clientes=linkTo(methodOn(this.getClass()).obtenerClientes(1,100)).withRel("TodosLosClientes");
            final var cliente = this.servicioCliente.obtenerCliente(documento);
            return EntityModel.of(cliente).add(self,cuentas,clientes);

        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

            }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> reemplazarUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> reemplazarUnCliente("12345678", DATOS)
    @PutMapping("/{documento}")
    public void reemplazarUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.guardarCliente(cliente);
    }

    // PUT http://localhost:9000/api/v1/clientes/{documento} + DATOS -> emparcharUnCliente(documento, DATOS)
    // PUT http://localhost:9000/api/v1/clientes/12345678 + DATOS -> emparcharUnCliente("12345678", DATOS)
    @PatchMapping("/{documento}")
    public void emparacharUnCliente(@PathVariable("documento") String nroDocumento,
                                    @RequestBody Cliente cliente) {
        cliente.documento = nroDocumento;
        this.servicioCliente.emparcharCliente(cliente);
    }

    // DELETE http://localhost:9000/api/v1/clientes/{documento}
    // DELETE http://localhost:9000/api/v1/clientes/12345678 + DATOS
    @DeleteMapping("/{documento}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borraCliente(@PathVariable String documento) {
        try {
            this.servicioCliente.borrarCliente(documento);
        } catch (Exception x) {
        }
    }
}
