package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    public List<Cuenta> obtenerCuentas();

    //CREAR
    public void insertarCuentaNueva(Cuenta cuenta);

    //READ
    public Cuenta obtenerCuenta(String numero);

    //UPDATE
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    //DELETE
    public  void borrarCuenta(String numero);

    //CRUD PARA CLIENTES CUENTAS
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta);
    public void modificarCuentaCliente(String documento, Cuenta nroCuenta);
    public void emparcharCuentaCliente(String documento, Cuenta nroParche);

    public void eliminarCuentaCliente(String documento, String numeroCuenta);


}
