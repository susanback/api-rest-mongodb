package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cuenta;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RepositorioCuenta extends MongoRepository<Cuenta,String> {

    public Optional<Cuenta> findByNumero(String numero);
    public void deleteByNumero(String numero);

}
