package mis.pruebas.apirest.servicios.repositorios;

import mis.pruebas.apirest.modelos.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RepositorioCliente extends MongoRepository<Cliente,String> {

    public Optional<Cliente> findByDocumento(String documento);
    public void deleteByDocumento(String documento);
}
