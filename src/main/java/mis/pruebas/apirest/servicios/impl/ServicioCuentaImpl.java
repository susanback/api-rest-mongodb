package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    @Autowired
    RepositorioCuenta repositorioCuenta;

    @Autowired
    ServicioCliente servicioCliente;


    @Override
    public List<Cuenta> obtenerCuentas() {
        return this.repositorioCuenta.findAll();
    }

    @Override
    public void insertarCuentaNueva(Cuenta cuenta) {
        this.repositorioCuenta.insert(cuenta);

    }

    @Override
    public Cuenta obtenerCuenta(String numero) {
        final var  quizasCuenta= this.repositorioCuenta.findByNumero(numero);
        if(!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta número" + numero);
        return quizasCuenta.get();
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if(!this.repositorioCuenta.existsById(cuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + cuenta.numero);
        this.repositorioCuenta.save(cuenta);

    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        if(!this.repositorioCuenta.existsById(parche.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + parche.numero);

        final Cuenta existente = obtenerCuenta(parche.numero);

        if(parche.numero != null)
            existente.numero = parche.numero;

        if(parche.moneda != null)
            existente.moneda = parche.moneda;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if (parche.saldo != null)
            existente.saldo = parche.saldo;

        if (parche.oficina != null)
            existente.oficina = parche.oficina;

        if (parche.tipo != null)
            existente.tipo = parche.tipo;


        this.repositorioCuenta.save(existente);

    }

    @Override
    public void borrarCuenta(String numero) {
        if(!this.repositorioCuenta.existsById(numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + numero);
        this.repositorioCuenta.deleteByNumero(numero);

    }

    @Override
    public Cuenta obtenerCuentaCliente(String documento, String numeroCuenta) {
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + numeroCuenta);
        final var  cliente = this.servicioCliente.obtenerCliente(documento);

        if(!cliente.codigoCuenta.contains(numeroCuenta))
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + numeroCuenta);
        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if(!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("No existe la cuenta número" + numeroCuenta);
        return quizasCuenta.get();

    }

    @Override
    public void eliminarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        final var quizasCuenta = this.repositorioCuenta.findById(numeroCuenta);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + numeroCuenta);
        final var cuenta = quizasCuenta.get();
        cuenta.estado = "INACTIVA";
        cliente.codigoCuenta.remove(numeroCuenta);
        guardarCuenta(cuenta);
        this.servicioCliente.guardarCliente(cliente);

    }

    @Override
    public void modificarCuentaCliente(String documento, Cuenta nroCuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        if(!this.repositorioCuenta.existsById(nroCuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + nroCuenta.numero);
        final var quizasCuenta = this.repositorioCuenta.findById(nroCuenta.numero);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + nroCuenta.numero);
        guardarCuenta(nroCuenta);
        this.servicioCliente.guardarCliente(cliente);
    }

    @Override
    public void emparcharCuentaCliente(String documento, Cuenta nroCuenta) {
        final Cliente cliente = this.servicioCliente.obtenerCliente(documento);
        if(!this.repositorioCuenta.existsById(nroCuenta.numero))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + nroCuenta.numero);
        final var quizasCuenta = this.repositorioCuenta.findById(nroCuenta.numero);
        if (!quizasCuenta.isPresent())
            throw new ObjetoNoEncontrado("El cliente no tiene esta cuenta asociada: " + nroCuenta.numero);
        emparcharCuenta(nroCuenta);
        this.servicioCliente.guardarCliente(cliente);
    }
}
