package mis.pruebas.apirest.servicios.impl;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.servicios.ObjetoNoEncontrado;
import mis.pruebas.apirest.servicios.ServicioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCliente;
import mis.pruebas.apirest.servicios.repositorios.RepositorioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente  {

    @Autowired
    RepositorioCliente repositorioCliente;

    @Autowired
    RepositorioCuenta repositorioCuenta;


    @Override
    public Page<Cliente> obtenerClientes(int pagina, int cantidad) {

        return this.repositorioCliente.findAll(PageRequest.of(pagina, cantidad));
    }

    @Override
    public void insertarClienteNuevo(Cliente cliente) {

        this.repositorioCliente.insert(cliente);

    }

    @Override
    public Cliente obtenerCliente(String documento) {

        final Optional<Cliente> quizasCliente = this.repositorioCliente.findByDocumento(documento);
        if(!quizasCliente.isPresent())
            throw new ObjetoNoEncontrado("No existe el cliente con documento " + documento);
        return quizasCliente.get();
    }

    @Override
    public void guardarCliente(Cliente cliente) {
        if(!this.repositorioCliente.existsById(cliente.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento" + cliente.documento);
        this.repositorioCliente.save(cliente);

    }

    @Override
    public void emparcharCliente(Cliente parche) {
        if(!this.repositorioCliente.existsById(parche.documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento" + parche.documento);

        final Cliente existente = obtenerCliente(parche.documento);

        if(parche.documento != null)
            existente.documento = parche.documento;

        if(parche.edad != null)
            existente.edad = parche.edad;

        if(parche.nombre != null)
            existente.nombre = parche.nombre;

        if(parche.correo != null)
            existente.correo = parche.correo;

        if(parche.direccion != null)
            existente.direccion = parche.direccion;

        if(parche.telefono != null)
            existente.telefono = parche.telefono;

        if(parche.fechaNacimiento != null)
            existente.fechaNacimiento = parche.fechaNacimiento;

        this.repositorioCliente.save(existente);


    }

    @Override
    public void borrarCliente(String documento) {
        if(!this.repositorioCliente.existsById(documento))
            throw new ObjetoNoEncontrado("No existe el cliente con documento" + documento);

        this.repositorioCliente.deleteByDocumento(documento);

    }

    @Override
    public void agregarCuentaCliente(String documento, String numeroCuenta) {
        final Cliente cliente = obtenerCliente(documento);
        if(!this.repositorioCuenta.existsById(numeroCuenta))
            throw new ObjetoNoEncontrado("No existe la cuenta número" + numeroCuenta);

        cliente.codigoCuenta.add(numeroCuenta);
        this.repositorioCliente.save(cliente);

    }

    @Override
    public List<String> obtenerCuentasCliente(String documento) {
        final Cliente cliente = obtenerCliente(documento);
        return cliente.codigoCuenta;
    }
}
