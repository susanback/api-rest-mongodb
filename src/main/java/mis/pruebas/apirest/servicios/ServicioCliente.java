package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cliente;
import mis.pruebas.apirest.modelos.Cuenta;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ServicioCliente {

    // CRUD

    public Page<Cliente> obtenerClientes(int pagina, int cantidad);

    // CREATE
    public void insertarClienteNuevo(Cliente cliente);

    // READ
    public Cliente obtenerCliente(String documento);

    // UPDATE (solamente modificar, no crear).
    public void guardarCliente(Cliente cliente);
    public void emparcharCliente(Cliente parche);

    // DELETE
    public void borrarCliente(String documento);


    // METODOS DE CLIENTEST EN RELACION A CUENTAS
    public void agregarCuentaCliente(String documento, String numeroCuenta);

    //CONSULTA
    public List<String> obtenerCuentasCliente(String documento);


}
