package mis.pruebas.apirest.modelos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

public class Cliente {

    @Id
    public String documento;
    public String nombre;
    public String edad;
    public String fechaNacimiento;
    public String telefono;
    public String correo;
    public String direccion;

    //a pesar de que no ingreses el campo no lo setee a null
    @JsonIgnore
    public List<String> codigoCuenta = new ArrayList<>();

}
